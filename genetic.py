"""A simple Pyevolve-based driver for OpenMDAO."""

import re
import random
from deap import tools

#pyevolve calls multiprocessing.cpu_count(), which can raise NotImplementedError
#so try to monkeypatch it here to return 1 if that's the case
try:
    import multiprocessing
    multiprocessing.cpu_count()
except ImportError:
    pass
except NotImplementedError:
    multiprocessing.cpu_count = lambda: 1
import barecmaes2 as cma # download: wget https://www.lri.fr/~hansen/barecmaes2.py
from pyevolve import G1DList, GAllele, GenomeBase, Scaling
from deap import base, creator
from pyevolve import GSimpleGA, Selectors, Initializators, Mutators, Consts

# pylint: disable-msg=E0611,F0401
from openmdao.main.datatypes.api import Enum, Float, Int, Bool, Slot

from openmdao.main.api import Driver
from openmdao.main.hasparameters import HasParameters
from openmdao.main.hasobjective import HasObjective
from openmdao.main.hasevents import HasEvents
from openmdao.main.interfaces import IHasParameters, IHasObjective, \
                                     implements, IOptimizer
from openmdao.util.decorators import add_delegate
from openmdao.util.typegroups import real_types, int_types, iterable_types

array_test = re.compile("(\[[0-9]+\])+$")


@add_delegate(HasParameters, HasObjective, HasEvents)
class Genetic(Driver):
    """Genetic algorithm for the OpenMDAO framework, based on the Pyevolve
    Genetic algorithm module.
    """

    implements(IHasParameters, IHasObjective, IOptimizer)

    # pylint: disable-msg=E1101
    opt_type = Enum("minimize", values=["minimize", "maximize"],
                    iotype="in",
                    desc='Sets the optimization to either minimize or maximize '
                         'the objective function.')

    generations = Int(Consts.CDefGAGenerations, iotype="in",
                      desc="The maximum number of generations the algorithm "
                           "will evolve to before stopping.")
    population_size = Int(Consts.CDefGAPopulationSize, iotype="in",
                          desc="The size of the population in each generation.")
    crossover_rate = Float(Consts.CDefGACrossoverRate, iotype="in", low=0.0,
                           high=1.0, desc="The crossover rate used when two "
                          "parent genomes reproduce to form a child genome.")
    mutation_rate = Float(Consts.CDefGAMutationRate, iotype="in", low=0.0,
                          high=1.0, desc="The mutation rate applied to "
                                         "population members.")

    selection_method = Enum("roulette_wheel",
                            ("roulette_wheel",
                             #"tournament", #this seems to be broken
                             "rank",
                             "uniform"),
                            desc="The selection method used to pick population "
                                 "members who will survive for "
                                 "breeding into the next generation.",
                            iotype="in")
    _selection_mapping = {"roulette_wheel":Selectors.GRouletteWheel,
                          #"tournament":Selectors.GTournamentSelector,
                          #this does not seem to function right for pyevolve
                          "rank":Selectors.GRankSelector,
                          "uniform":Selectors.GUniformSelector}

    elitism = Bool(False, iotype="in", desc="Controls the use of elitism in "
                                            "the creation of new generations.")

    best_individual = Slot(klass=GenomeBase.GenomeBase,
                           desc="The genome with the "
                                "best score from the optimization.")

    seed = Int(None, iotype="in",
               desc="Random seed for the optimizer. Set to a specific value "
                    "for repeatable results; otherwise leave as None for truly "
                    "random seeding.")

    def execute(self):
        """Perform the optimization"""
        self.set_events()

        initial_guess = []
        parameters = self.get_parameters()
        pnames = parameters.keys()
        for param in self.get_parameters().values():
            val = param.evaluate()[0]
            initial_guess.append(val)
        
        #GO
        res = cma.fmin(self._run_model, initial_guess, 0.3, verb_disp=100, verb_plot=0)

        best_individual = res[0] # ga.bestIndividual()

        #run it once to get the model into the optimal state
        self._run_model(best_individual)

    def _run_model(self, chromosome):
        self.set_parameters([val for val in chromosome])
        self.run_iteration()
        return self.eval_objective()

