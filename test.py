import numpy as np
from openmdao.main.api import Component, Assembly
#outer_opt()from openmdao.lib.drivers.api import SLSQPdriver, CONMINdriver, Genetic, COBYLAdriver, NEWSUMTdriver
from genetic import Genetic
from openmdao.lib.datatypes.api import Float
import unittest

class rosen(Component):
    x1 = Float(0.0, iotype='in', desc = 'The variable x1' )
    x2 = Float(0.0, iotype='in', desc = 'The variable x2' )
    f = Float(0.0, iotype='out', desc='F(x,y)')

    def execute(self):
        x1 = self.x1
        x2 = self.x2
        self.f = (1-x1)**2 + 100*(x2-x1**2)**2 + 1e-5*np.random.random()

class outer_opt(Assembly):
    def configure(self):
        self.driver = self.add('driver', Genetic())

        self.add('rose',rosen())

        self.driver.workflow.add('rose')
        self.add('x2',Float(0.4, iotype = 'in'))
        self.driver.add_parameter('x2', low = -6, high = 6)
        self.connect('x2', 'rose.x2')
        self.add('x1',Float(0.4, iotype = 'in'))
        self.driver.add_parameter('x1', low = -6, high = 6)
        self.connect('x1', 'rose.x1')
        self.driver.add_objective('rose.f')

class TestOUU(unittest.TestCase):
    def test(self):
        top = outer_opt()
        top.run()
        self.assertTrue(abs(top.x1-1) + abs(top.x2-1) < 0.01)

if __name__ == '__main__':
    unittest.main()
